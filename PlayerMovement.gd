extends KinematicBody2D

export (int) var speed = 200

onready var sprite = get_node("Player/Sprite")

var velocity = Vector2()
var anim = "idle"

func get_input():
    velocity = Vector2()
    if Input.is_action_pressed('ui_right'):
        velocity.x += 1
    if Input.is_action_pressed('ui_left'):
        velocity.x -= 1
    if Input.is_action_pressed('ui_down'):
        velocity.y += 1
    if Input.is_action_pressed('ui_up'):
        velocity.y -= 1
    velocity = velocity.normalized() * speed

func _physics_process(delta):
    get_input()
    move_and_slide(velocity)
		
func _fixed_process(delta):
	if abs(velocity.x) < 10:
        velocity.x = 0
	
	#set animations
	if velocity.x <= 1:
		anim = "idle"
	else:
		anim = "walking"
	if velocity.x > 0:
		sprite.set_flip_h(false)
	elif velocity.x <= 1:
		sprite.set_flip_h(true)
		
	sprite.play(anim)
	print(PROGRESS.variables)

#Next Scene button
func _on_NextScene_pressed():
	get_tree().change_scene("res://Spring.tscn")
	pass # Replace with function body.

func _on_BackToStart_pressed():
	get_tree().change_scene("res://TitleScreen.tscn")
