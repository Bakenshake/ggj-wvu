extends Control

onready var dialogue = get_node("../Dialogue")

func _ready():
	# Connect all the signals for the buttons
	$Continue.connect("pressed", self, "press_next")
	$InitiateA.connect("pressed", self, "init_a")
	$Kitchen.connect("pressed", self, "start_potluck")
	$KitchenNext.connect("pressed", self, "press_next")

func press_next():
	dialogue.next()

func init_a():
	dialogue.initiate("openingscene5")
	$InitiateA.hide()
	$Continue.show()
	
func start_potluck():
	dialogue.initiate("potluckprep")
	$Kitchen.hide()
	$KitchenNext.show()
	pass # Replace with function body.

func clear():
	PROGRESS.variables = {}
	PROGRESS.dialogues = {}

func set_vars():
	if $Var1.text:
		PROGRESS.variables['var1'] = $Var1.text
	if $Var2.text:
		PROGRESS.variables['var2'] = int($Var2.text)
	if $Var3.text:
		PROGRESS.variables['var3'] = int($Var3.text)
	if $Var4.text:
		PROGRESS.variables['var4'] = int($Var4.text)
	if $Var5.text:
		PROGRESS.variables['var5'] = int($Var5.text)


