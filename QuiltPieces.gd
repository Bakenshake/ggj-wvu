extends Control

func _ready():
	pass # Replace with function body.

func _on_Cardinal_pressed():
	get_node("CardinalPopup").popup()
	pass # Replace with function body.

func _on_Coal_pressed():
	get_node("CoalPopup").popup()
	pass # Replace with function body.

func _on_Rhododendron_pressed():
	get_node("RhododendronPopup").popup()
	pass # Replace with function body.

func _on_Horsehoe_pressed():
	get_node("HorseshoePopup").popup()
	pass # Replace with function body.

func _on_Moonshine_pressed():
	get_node("MoonshinePopup").popup()
	pass # Replace with function body

func _on_Mothman_pressed():
	get_node("MothmanPopup").popup()
	pass # Replace with function body.
